package code.comet

import net.liftweb.common.Loggable
import net.liftweb.http.js.JE
import net.liftweb.http.js.JsCmds.{Noop, SetValById}
import net.liftweb.http.{CometActor, SHtml}
import net.liftweb.json.{DefaultFormats, JValue}

class ChatForm extends CometActor with Loggable {

  implicit val formats = DefaultFormats

  private def sendChatMsg(value: JValue) = {
    value.extractOpt[ChatMsg] match {
      case Some(msg) => {
        logger.info(s"Received Chat Msg: $msg")
        ChatServer ! msg.copy(sent = Some(System.currentTimeMillis))
        SetValById("chat_msg", "")
      }
      case _ => Noop
    }
  }

  def render = {
    "#send_msg [onclick]" #> SHtml.jsonCall(JE.Call("getChatMsg"), sendChatMsg _ )
  }
}
