package code.comet

import net.liftweb._
import http._
import actor._

case class ChatMsg(chat_user: String, chat_msg: String, sent: Option[Long])

object ChatServer extends LiftActor with ListenerManager {
  private var msgs: Vector[ChatMsg] = Vector(ChatMsg("SysAdmin", "Welcome",
    Some(System.currentTimeMillis)))

  def createUpdate = msgs

  override def lowPriority = {
    case msg: ChatMsg => {
      if (msg.chat_msg.nonEmpty && msg.chat_user.nonEmpty) {
        msgs :+= msg
        updateListeners()
      }
    }
  }
}
