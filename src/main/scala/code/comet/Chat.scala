package code.comet

import net.liftweb._
import http._

class Chat extends CometActor with CometListener {

  lazy val colors = List("green", "orange", "red", "blue")
  lazy val dtFormat = new java.text.SimpleDateFormat("HH:mm:ss")

  private var msgs: Vector[ChatMsg] = Vector()

  def registerWith = ChatServer

  override def lowPriority = {
    case v: Vector[ChatMsg] => {
      msgs = v
      reRender()
    }
  }

  def msgSentAsDate(sent: Option[Long]) = sent.map { s => dtFormat.format(new java.util.Date(s)) }.getOrElse("")

  def render = "#chatMsgs *" #> msgs.toList.zipWithIndex.map {
    case (m, idx) => {
      val msgColor = colors(idx % colors.size)
      (m, idx, msgColor)
    }}.sortBy(_._1.sent.getOrElse(0L) * -1).map {
      case (m, idx, msgColor) => {
      "#chatUser *" #> s"${m.chat_user}  [${msgSentAsDate(m.sent)}]: " &
        "#chatMsg *" #> m.chat_msg &
        "#chatMsg [style+]" #> s"color: $msgColor;"
    }
  }
}


