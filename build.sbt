name := "chat-app-demo"

version := "0.0.1"

organization := "net.liftweb"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "releases"  at "https://oss.sonatype.org/content/repositories/releases"
)

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
  val liftVersion = "2.6.2"
  val liftEdition = liftVersion.substring(0, 3)
  Seq(
    "net.liftweb"             %% "lift-webkit"                        % liftVersion           % "compile",
    "net.liftweb"             %% "lift-mapper"                        % liftVersion           % "compile",
    "net.liftmodules"         %% ("lift-jquery-module_"+liftEdition)  % "2.8"                 % "compile",
    "net.liftweb"             %% "lift-mongodb-record"                % liftVersion,
    "org.eclipse.jetty"       %  "jetty-webapp"                       % "9.2.7.v20150116"     % "compile",
    "org.eclipse.jetty"       %  "jetty-plus"                         % "9.2.7.v20150116"     % "container,test", // For Jetty Config
    "org.eclipse.jetty.orbit" %  "javax.servlet"                      % "3.0.0.v201112011016" % "container,test" artifacts Artifact("javax.servlet", "jar", "jar"),
    "ch.qos.logback"          %  "logback-classic"                    % "1.0.11"              % "compile",
    "ch.qos.logback"          %  "logback-core"                       % "1.0.11"              % "compile",
    "org.slf4j"               % "slf4j-api"                           % "1.7.4"               % "compile",
    "org.specs2"              %% "specs2"                             % "2.3.12"              % "test",
    "com.h2database"          %  "h2"                                 % "1.4.187",
    "com.typesafe.akka"       %  "akka-actor_2.11"                    % "2.5.4",
    "net.databinder.dispatch" %% "dispatch-core"                      % "0.13.1"
  )
}

// Enable the jetty plugin
enablePlugins(JettyPlugin)

// Enable heroku plugin
enablePlugins(HerokuDeploy)

bashScriptConfigLocation := Some("${app_home}/../conf/jvmopts")

mainClass in Compile := Some("bootstrap.liftweb.Start")

herokuAppName := "scala-lift-demo-chat-app"

// Enable AWS Elastic Beanstalk plugin

enablePlugins(ElasticBeanstalkDeployPlugin)

elasticBeanstalkAppName := "scala-lift-chat-app"

elasticBeanstalkEnvName := "scalaLiftChatApp-prod"

elasticBeanstalkRegion  := "us-east-1"