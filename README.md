chat-app-demo
==============

Google Slides:  https://docs.google.com/presentation/d/1WNIEDZ9Ws4gwXY-tUgSMpEr665BShnL9oh4P8MioopU/edit?usp=sharing

Running locally
===============

You will need Java Virtual Machine (JVM) 8+ to run


```

git clone https://bitbucket.org/vickumar/chat-app-demo

cd chat-app-demo/

./sbt update

./sbt ~jetty:start
 
```

The application should be running locally on http://localhost:8080


Running with Ngrok
===================

Download and install ngrok:  https://ngrok.com/

Run application locally: `./sbt ~jetty:start`

Start a tunnel to your local machine: `ngrok http 8080`



Deploy to Heroku
=================

We will be taking advantage of the xsbt-web-plugin:  https://github.com/earldouglas/xsbt-web-plugin/blob/master/docs/4.0.x.md


Either:

  - Download and install Heroku Toolbelt:  https://devcenter.heroku.com/articles/heroku-cli

or

  - Set your Heroku API Key: `HEROKU_API_KEY="xxx-xxx-xxxx" sbt`


Add settings to your `build.sbt` file:

```
enablePlugins(HerokuDeploy)

herokuAppName := "my-heroku-app"
```

 
Deploy:

  - `./sbt herokuDeploy`


Deploy to AWS Elastic Beanstalk
================================

We will be taking advantage of the same xsbt-web-plugin:  https://github.com/earldouglas/xsbt-web-plugin/blob/master/docs/4.0.x.md


Optional:

  -  Download and install AWS CLI:  https://aws.amazon.com/cli/

Required:

  -  Set your AWS Credentials in the environment: `AWS_ACCESS_KEY="xxx" AWS_SECRET_KEY="xxx" sbt`


Add settings to your `build.sbt` file:

```
enablePlugins(ElasticBeanstalkDeployPlugin)

elasticBeanstalkAppName := "scala-lift-chat-app"

elasticBeanstalkEnvName := "scalaLiftChatApp-prod"

elasticBeanstalkRegion  := "us-east-1"
```


Deploy:

  - `./sbt elasticBeanstalkDeploy`
  

Testing with Docker Locally
============================

Install docker:  https://docs.docker.com/install/

Build and Deploy locally using Docker:

  -  Build your docker image: `docker build -t [tag] .`
  -  Run the app using docker: `docker run -p 8080:8080 [tag]`
  -  Navigate to `http;//localhost:8080` and verify the application is working
  

Deploying to Google Cloud Platform
===================================

Google Cloud Platform Setup:

  -  Install Google Cloud SDK:  https://cloud.google.com/sdk/docs/quickstarts
  -  Set Google Cloud Engine Zone: `gcloud config set compute/zone us-central1-b`
  -  Using the `gloud` command install Kubernetes: `gcloud components install kubectl`
  -  Set current project: `export PROJECT_ID="$(gcloud config get-value project -q)"`

Build and Upload:

  -  Build the container image:  `docker build -t gcr.io/${PROJECT_ID}/chat-app-demo:v1  .`
  -  Upload the container image:  `gcloud docker -- push gcr.io/${PROJECT_ID}/chat-app-demo:v1`

Creating a new cluster:

  -  Create a container cluster: `gcloud container clusters create chat-app-demo --num-nodes=1`
  -  List container instances: `gcloud compute instances list`
  -  Deploy the app: `kubectl run chat-app-demo --image=gcr.io/${PROJECT_ID}/chat-app-demo:v1 --port 8080`
  -  List pods: `kubectl get pods`
  
Exposing to the internet:

  -  Expose to internet: `kubectl expose deployment chat-app-demo --type=LoadBalancer --port 80 --target-port 8080`
  -  Check services: `kubectl get service`
  
The External IP can be used to access your application.

Scaling Up/Down:

  -  Scale up/down: `kubectl scale deployment chat-app-demo --replicas=3`

Redeploying:
```
docker build -t gcr.io/${PROJECT_ID}/chat-app-demo:v2 .
gcloud docker -- push gcr.io/${PROJECT_ID}/chat-app-demo:v2
kubectl set image deployment/chat-app-demo chat-app-demo=gcr.io/${PROJECT_ID}/chat-app-demo:v2
```

Cleanup:
```
kubectl delete service chat-app-demo
Wait for load balancer to delete by checking:
  gcloud compute forwarding-rules list
gcloud container clusters delete chat-app-demo
```

